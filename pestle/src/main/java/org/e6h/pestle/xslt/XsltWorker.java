package org.e6h.pestle.xslt;

import org.e6h.pestle.Worker;

public class XsltWorker extends Worker {
  public XsltWorker() {
    super(new XsltProcessor());
  }

  public static void main(String[] args) {
    try {
      Worker w = new XsltWorker();
      w.run(args);
    }
    catch (Exception ex) {
      ex.printStackTrace();
      System.exit(1);
    }
  }
}
