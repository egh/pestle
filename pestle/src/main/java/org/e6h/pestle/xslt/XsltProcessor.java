package org.e6h.pestle.xslt;

import java.util.List;
import java.io.File;
import javax.xml.transform.stream.StreamSource;
import net.sf.saxon.s9api.DocumentBuilder;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.Configuration;
import net.sf.saxon.s9api.XdmValue;
import net.sf.saxon.s9api.XsltCompiler;
import net.sf.saxon.s9api.XsltExecutable;
import net.sf.saxon.s9api.XsltTransformer;
import net.sf.saxon.trans.XmlCatalogResolver;
import org.apache.xml.resolver.CatalogManager;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import org.e6h.pestle.Processor;

class XsltProcessor implements Processor {
  private String lastXsltPath;
  private String lastCatalogPath;
  private net.sf.saxon.s9api.Processor proc;
  private Configuration config;
  private XsltCompiler xsltCompiler;
  private XsltExecutable exec;
  private XsltTransformer trans;
  private Serializer serializer;

  @Override
  public void processRequest(List<String> args) throws Exception {
    String xsltPath = args.get(0);
    String catalogPath = args.get(1);
    String inputPath = args.get(2);
    String outputPath = args.get(3);

    if (!new File(xsltPath).exists()) throw new Exception();
    if (!new File(catalogPath).exists()) throw new Exception();

    if (!xsltPath.equals(lastXsltPath) || !catalogPath.equals(lastCatalogPath)) {
      System.out.println("regenerating" + lastXsltPath + xsltPath);
      lastXsltPath = xsltPath;
      lastCatalogPath = catalogPath;
      proc = new net.sf.saxon.s9api.Processor(false);
      config = proc.getUnderlyingConfiguration();
      XmlCatalogResolver.setCatalog(catalogPath, config, false);
      xsltCompiler = proc.newXsltCompiler();
      exec = xsltCompiler.compile(new StreamSource(new FileInputStream(new File(xsltPath))));
      trans = exec.load();
      serializer = proc.newSerializer();
      serializer.setOutputProperty(Serializer.Property.METHOD, "xml");
      serializer.setOutputProperty(Serializer.Property.OMIT_XML_DECLARATION,"yes");
    }

    File inputFile = new File(inputPath);
    File outputFile = new File(outputPath);

    FileInputStream is = new FileInputStream(inputFile);
    FileOutputStream os = new FileOutputStream(outputFile);
    try {
      serializer.setOutputStream(os);
      trans.setDestination(serializer);
      trans.setSource(new StreamSource(is));
      trans.transform();
      trans.close();
      serializer.close();
    } finally {
      is.close();
      os.close();
    }
  }
}
