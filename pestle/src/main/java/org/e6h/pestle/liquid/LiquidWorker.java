package org.e6h.pestle.liquid;

import org.e6h.pestle.Worker;

public class LiquidWorker extends Worker {
  public LiquidWorker() {
    super(new LiquidProcessor());
  }

  public static void main(String[] args) {
    try {
      Worker w = new LiquidWorker();
      w.run(args);
    }
    catch (Exception ex) {
      ex.printStackTrace();
      System.exit(1);
    }
  }
}
