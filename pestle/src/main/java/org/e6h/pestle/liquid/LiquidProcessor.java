package org.e6h.pestle.liquid;

import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.commonmark.Extension;
import org.commonmark.ext.front.matter.YamlFrontMatterExtension;
import org.commonmark.ext.front.matter.YamlFrontMatterVisitor;
import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.e6h.pestle.Processor;

import liqp.Template;

class LiquidProcessor implements Processor {
  private static final Set<Extension> EXTENSIONS = Collections.singleton(YamlFrontMatterExtension.create());
  private static final Parser PARSER = Parser.builder().extensions(EXTENSIONS).build();
  private static final HtmlRenderer RENDERER = HtmlRenderer.builder().extensions(EXTENSIONS).build();
  private Template template;
  private String lastTemplatePath;

  public void processRequest(List<String> args) throws Exception {
    String templatePath = args.get(0);
    String inputPath = args.get(1);
    String outputPath = args.get(2);

    if (!templatePath.equals(lastTemplatePath)) {
      template = Template.parse(new File(templatePath));
    }

    YamlFrontMatterVisitor visitor = new YamlFrontMatterVisitor();
    FileReader reader = new FileReader(new File(inputPath));
    Node document = PARSER.parseReader(reader);
    document.accept(visitor);
    Map<String, List<String>> metadata =  visitor.getData();
    Map<String, Object> data = new HashMap<>();
    Map<String, Object> post = new HashMap<>();
    data.put("post", post);
    post.put("content", RENDERER.render(document));
    post.put("metadata", metadata);
    try (PrintWriter out = new PrintWriter(outputPath)) {
      out.println(template.render(data));
    }
  }
}
