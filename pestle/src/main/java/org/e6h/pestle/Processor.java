package org.e6h.pestle;

import java.util.List;

public interface Processor {
  void processRequest(List<String> args) throws Exception;
}
