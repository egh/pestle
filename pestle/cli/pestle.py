import argparse
import subprocess
import webbrowser
def build(args):
    subprocess.call(["bazel", "build", ":all"])
    webbrowser.open("http://example.org", new=0)


parser = argparse.ArgumentParser(description='Pestle')
subparsers = parser.add_subparsers(help='sub-command help')
parser_build = subparsers.add_parser('build', help='build help')
parser_build.set_defaults(func=build)
args = parser.parse_args()
args.func(args)
