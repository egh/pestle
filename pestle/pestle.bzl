# -*- bazel -*-
load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

# Group a list into groups of size n
def _group(rest, n):
    retval = [[]]
    m = 0
    for item in rest:
        if m == n:
            m = 0
            retval.append([])
        m += 1
        retval[-1].append(item)
    return retval

def _batch_genrule(name, srcs, suffix, tools, cmd, n):
    groups = _group(srcs, n)
    retval = []
    for counter, group in enumerate(groups):
        outs = ["%s%s"%(src, suffix) for src in group ]
        retval += outs
        native.genrule(
            name = "%s_%s_%d"%(name, suffix, counter),
            tools = tools,
            srcs = group,
            outs = outs,
            cmd = cmd
        )
    return retval

def transform_batch(srcs, xslt, catalog, suffix, deps=[], n=100):
    transform_label = Label('//pestle:transform')
    return _batch_genrule(name = "transform",
                          srcs = srcs,
                          suffix = suffix,
                          tools = [transform_label, xslt, catalog] + deps + native.glob(["dtd/**"]),
                          cmd = "$(location %s) %s %s $(SRCS) $(OUTS)"%(transform_label, xslt, catalog),
                          n = n)

def concat(srcs, head, foot):
    retval = []
    for src in srcs:
        out = "%s.html"%(src)
        retval.append(out)
        native.genrule(
            name = "concat_%s"%(out),
            tools = [head, foot],
            outs = [out],
            srcs = [src],
            cmd = "cat %s $(<) %s > $(@)"%(head, foot)
        )
    return retval

def index(srcs, collection, n=100):
    groups = _group(srcs, n)
    for counter, group in enumerate(groups):
        outs = ["%s.indexed"%(src) for src in group ]
        native.genrule(
            name = "index_%d"%(counter),
            tools = ["solr-core-6.5.0.jar"],
            srcs = group,
            outs = outs,
            cmd = "java -classpath solr-core-6.5.0.jar -Dauto=yes -Dc=%s -Ddata=files org.apache.solr.util.SimplePostTool $(SRCS) && touch $(OUTS) "%(collection)
            )

def pandoc_batch(srcs, to_format="html", n=100):
    pandoc_label = Label('//pestle:pandoc_batch')
    return _batch_genrule(
        name = "pandoc",
        tools = [pandoc_label],
        srcs = srcs,
        suffix = ".%s"%(to_format),
        cmd = "$(location %s) --to=%s $(SRCS) $(OUTS)"%(pandoc_label, to_format),
        n = n)

def pandoc_extract_metadata_batch(srcs, n=100):
    tpl_label = Label('//pestle:pandoc_metadata_template')
    pandoc_label = Label('//pestle:pandoc_batch')
    return _batch_genrule(
        name = "pandoc_extract_metadata",
        tools = [pandoc_label, tpl_label],
        srcs = srcs,
        suffix = ".json",
        cmd = "$(location %s) --to=html --template=$(location %s) $(SRCS) $(OUTS)"%(pandoc_label, tpl_label),
        n = n)

def test_pestle():
    print("It works!")

def pestle_repositories():
    # The following dependencies were calculated from:
    # com.google.code.gson:gson:2.8.0
    native.maven_jar(
        name = "com_google_code_gson_gson",
        artifact = "com.google.code.gson:gson:2.8.0",
    )
    native.maven_jar(
        name = "nl_big_o_liqp",
        artifact = "nl.big-o:liqp:0.7.8",
        repository = "https://jcenter.bintray.com/",
        sha1 = "bc95a6e260b63ae3e45b1c3efcfd2917bd1e1dac",
    )
  
    # nl.big-o:liqp:jar:0.7.8
    native.maven_jar(
        name = "org_antlr_antlr4_runtime",
        artifact = "org.antlr:antlr4-runtime:4.7.1",
        repository = "https://jcenter.bintray.com/",
        sha1 = "946f8aa9daa917dd81a8b818111bec7e288f821a",
    )
  
  
    # nl.big-o:liqp:jar:0.7.8
    native.maven_jar(
        name = "com_fasterxml_jackson_core_jackson_databind",
        artifact = "com.fasterxml.jackson.core:jackson-databind:2.9.8",
        repository = "https://jcenter.bintray.com/",
        sha1 = "11283f21cc480aa86c4df7a0a3243ec508372ed2",
    )
  
  
    # nl.big-o:liqp:jar:0.7.8
    native.maven_jar(
        name = "org_jsoup_jsoup",
        artifact = "org.jsoup:jsoup:1.10.3",
        repository = "https://jcenter.bintray.com/",
        sha1 = "b842f960942503cf1abbcc8c173a7f2c19d43726",
    )
  
  
    # nl.big-o:liqp:jar:0.7.8
    # com.fasterxml.jackson.core:jackson-databind:bundle:2.9.8 got requested version
    native.maven_jar(
        name = "com_fasterxml_jackson_core_jackson_core",
        artifact = "com.fasterxml.jackson.core:jackson-core:2.9.8",
        repository = "https://jcenter.bintray.com/",
        sha1 = "0f5a654e4675769c716e5b387830d19b501ca191",
    )
  
  
    # com.fasterxml.jackson.core:jackson-databind:bundle:2.9.8 wanted version 2.9.0
    # nl.big-o:liqp:jar:0.7.8
    native.maven_jar(
        name = "com_fasterxml_jackson_core_jackson_annotations",
        artifact = "com.fasterxml.jackson.core:jackson-annotations:2.9.8",
        repository = "https://jcenter.bintray.com/",
        sha1 = "ba7f0e6f8f1b28d251eeff2a5604bed34c53ff35",
    )

    # The following dependencies were calculated from:
    # net.sf.saxon:Saxon-HE:9.7.0-18
    native.maven_jar(
        name = "net_sf_saxon_Saxon_HE",
        artifact = "net.sf.saxon:Saxon-HE:9.7.0-18",
    )

    # The following dependencies were calculated from:
    # xml-resolver:xml-resolver:1.2
    native.maven_jar(
        name = "xml_resolver",
        artifact = "xml-resolver:xml-resolver:1.2",
    )

    # proto_library, cc_proto_library, and java_proto_library rules implicitly
    # depend on @com_google_protobuf for protoc and proto runtimes.
    # This statement defines the @com_google_protobuf repo.
    http_archive(
        name = "com_google_protobuf",
        sha256 = "9510dd2afc29e7245e9e884336f848c8a6600a14ae726adb6befdb4f786f0be2",
        strip_prefix = "protobuf-3.6.1.3",
        urls = ["https://github.com/google/protobuf/archive/v3.6.1.3.zip"],
    )

    # java_lite_proto_library rules implicitly depend on @com_google_protobuf_javalite//:javalite_toolchain,
    # which is the JavaLite proto runtime (base classes and common utilities).
    http_archive(
        name = "com_google_protobuf_javalite",
        sha256 = "d8a2fed3708781196f92e1e7e7e713cf66804bd2944894401057214aff4f468e",
        strip_prefix = "protobuf-5e8916e881c573c5d83980197a6f783c132d4276",
        urls = ["https://github.com/google/protobuf/archive/5e8916e881c573c5d83980197a6f783c132d4276.zip"],
    )

    native.maven_jar(
        name = "com_atlassian_commonmark",
        artifact = "com.atlassian.commonmark:commonmark:0.12.1",
        sha1 = "9e0657f89ab2731f8a7235d926fdae7febf104cb",
    )

    native.maven_jar(
        name = "com_atlassian_commonmark_yaml",
        artifact = "com.atlassian.commonmark:commonmark-ext-yaml-front-matter:0.12.1",
        sha1 = "618500e7288e28abe73ba5eab9d2ea71a1bc2303",
    )

def render(out, template, data):
    render_label = Label('//pestle:render')
    tools = [render_label, template]
    args = " $(location %s) "%(template)
    srcs = []
    for k in data.keys():
        v = data[k]
        args += " %s=$(location %s) "%(k, v)
        srcs.append(v)
    native.genrule(
        name = "render_%s"%(out),
        tools = tools,
        srcs = srcs,
        outs = [out],
        cmd = "$(location %s) %s > $(@)"%(render_label, args))

def _s3_upload_impl(ctx):
    ctx.actions.run_shell(
        use_default_shell_env = True,
        inputs = [ctx.files.srcs[0]],
        command = "aws s3 cp '%s' '%s/%s'"%(ctx.files.srcs[0].path, ctx.attr.bucket, ctx.files.srcs[0].path),
        outputs = [ctx.outputs.out]
    )

s3_upload = rule(
    attrs = {
        "bucket": attr.string(mandatory = True),
        "srcs": attr.label_list(
            mandatory = True,
            allow_files = True,
        ),
    },
    outputs = {"out": "%{name}_uploaded"},
    implementation = _s3_upload_impl,
)

def s3_upload_batch(srcs, bucket):
    retval = []
    for src in srcs:
        name = "upload_%s"%(src)
        s3_upload(
            name = name,
            bucket = bucket,
            srcs = [src]
        )
        retval.append(name)
    return retval

def jq(srcs, out, filter, options=""):
    native.genrule(
        name = "jq_%s"%(out),
        srcs = srcs,
        outs = [out],
        cmd = "jq %s \"%s\" $(SRCS) > $(@)"%(options, filter))
    return out

def _xslt_impl(ctx):
    args = ctx.actions.args()
    args.add(ctx.file.template.path)
    args.add(ctx.file.catalog.path)
    args.add(ctx.files.srcs[0].path)
    args.add(ctx.outputs.out.path)
    args.use_param_file("@%s", use_always=True)

    inputs = [ctx.file.template, ctx.file.catalog, ctx.files.srcs[0]]
    inputs.extend(ctx.files.data)

    ctx.actions.run(
        inputs = inputs,
        mnemonic = "XSLT",
        outputs = [ctx.outputs.out],
        arguments = [args],
        execution_requirements = { "supports-workers": "1" },
        executable=ctx.executable._bazelxslt)

    return struct(orig=ctx.files.srcs[0], output=ctx.outputs.out)

xslt_single = rule(
    attrs = {
        "srcs": attr.label_list(
            mandatory = True,
            allow_files = True,
        ),
        "data": attr.label_list(
            allow_files = True,
        ),
        "_bazelxslt": attr.label(
            default = Label("//:XsltWorker"),
            executable = True,
            cfg = "host",
        ),
        "template": attr.label(allow_single_file = True),
        "catalog": attr.label(allow_single_file = True),
    },
    outputs = {
        "out": "%{name}.out",
    },
    implementation = _xslt_impl,
)

def xslt(srcs, data, catalog, template):
    retval = []
    for src in srcs:
        name = "transform_%s_%s"%(template, src)
        xslt_single(
            name = name,
            data = data,
            srcs = [src],
            catalog = "catalog.xml",
            template = "article-transform.xsl"
        )
        retval.append(name)
    return retval

def liquid(srcs, template):
    retval = []
    for src in srcs:
        name = "liquid_%s_%s"%(template, src)
        liquid_single(
            name = name,
            srcs = [src],
            template = template
        )
        retval.append(name)
    return retval

def _liquid_impl(ctx):
    args = ctx.actions.args()
    args.add(ctx.file.template.path)
    args.add(ctx.files.srcs[0].path)
    args.add(ctx.outputs.out.path)
    args.use_param_file("@%s", use_always=True)

    inputs = [ctx.file.template, ctx.files.srcs[0]]

    ctx.actions.run(
        inputs = inputs,
        mnemonic = "Liquid",
        outputs = [ctx.outputs.out],
        arguments = [args],
        execution_requirements = { "supports-workers": "1" },
        executable=ctx.executable._bazelliquid)

    return struct(orig=ctx.files.srcs[0], output=ctx.outputs.out)

liquid_single = rule(
    attrs = {
        "srcs": attr.label_list(
            mandatory = True,
            allow_files = True,
        ),
        "_bazelliquid": attr.label(
            default = Label("//:LiquidWorker"),
            executable = True,
            cfg = "host",
        ),
        "template": attr.label(allow_single_file = True),
    },
    outputs = {
        "out": "%{name}.html",
    },
    implementation = _liquid_impl,
)
