from loremipsum import get_sentence, get_paragraphs
for n in range(0, 100):
    f = open("test_%d.md"%(n), "w+")
    f.write("---\n")
    f.write("title: %s\n"%(get_sentence()))
    f.write("...\n")
    for p in get_paragraphs(5):
        f.write(p)
        f.write("\n\n")
    f.close()
