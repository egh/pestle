<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:preserve-space elements="*"/>

  <xsl:output
      method="xml"
      indent="no"
      encoding="UTF-8"/>

  <xsl:template match="/">
    <add>
      <doc>
        <field name="id">
          <xsl:value-of select="/article/front/article-meta/article-id[@pub-id-type='doi']"/>
        </field>
        <field name="title"><xsl:value-of select="/article/front/article-meta/title-group/article-title"/></field>
        <field name="body">
          <xsl:apply-templates select="/article/body"/>
        </field>
      </doc>
    </add>
  </xsl:template>

  <xsl:template match="text()">
    <xsl:text> </xsl:text>
    <xsl:value-of select="."/>
    <xsl:text> </xsl:text>
  </xsl:template>
</xsl:stylesheet>
