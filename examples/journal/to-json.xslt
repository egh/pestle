<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    version="3.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:preserve-space elements="*"/>

  <!-- using method=“json” double encodes stuff? -->
  <xsl:output
      method="text"
      indent="no"
      media-type="application/json"
      encoding="UTF-8"/>

  <xsl:template match="/">
    <xsl:variable name="doc">
      <map xmlns="http://www.w3.org/2005/xpath-functions">
        <string key="id">
          <xsl:value-of select="/article/front/article-meta/article-id[@pub-id-type='doi']"/>
        </string>
        <string key="title">
          <xsl:value-of select="/article/front/article-meta/title-group/article-title"/>
        </string>
        <string key="body">
          <xsl:apply-templates select="/article/body"/>
        </string>
      </map>
    </xsl:variable>
    <xsl:value-of select="xml-to-json($doc)"/>
  </xsl:template>

  <xsl:template match="text()">
    <xsl:text> </xsl:text>
    <xsl:value-of select="."/>
    <xsl:text> </xsl:text>
  </xsl:template>
</xsl:stylesheet>
